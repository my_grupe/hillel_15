import sqlite3
from flask import Flask, render_template, request, redirect

app = Flask(__name__)


@app.route("/")
def index():
    with sqlite3.connect('musik.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM song")
        musik = cursor.fetchall()
        print(musik)
    return render_template('index.html', musik=musik)


@app.route("/about/")
def about():
    return render_template('about.html')


@app.route("/add/", methods=['GET', "POST"])
def add_s():
    if request.method == 'POST':
        with sqlite3.connect("musik.db") as connection:
            query = f'INSERT INTO song(name, author, year) VALUES ("{request.form["name"]}",' \
                    f' "{request.form["author"]}",' \
                    f' "{request.form["year"]}")'
            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()

    return render_template('add_song.html')


@app.route("/delete/", methods=['GET', "POST"])
def delete_s():
    if request.method == 'POST':
        with sqlite3.connect("musik.db") as connection:
            query = f'DELETE FROM song WHERE id={request.form["song_id"]}'
            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
    return redirect('/')


# @app.route("/Edit/", methods=['GET', "POST"])
# def edit_s():
#     if request.method == 'POST':
#         with sqlite3.connect("musik.db") as connection:
#             query = f'INSERT INTO song(name, author, year) VALUES ("{request.form["name"]}",' \
#                     f' "{request.form["author"]}",' \
#                     f' "{request.form["year"]}")'
#             cursor = connection.cursor()
#             cursor.execute(query)
#             connection.commit()


if __name__ == '__main__':
    app.run(debug=True)
